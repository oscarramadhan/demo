package com.dubbo.test.pickup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PickupApplication {
	public static void main(String[] args) {
		System.out.println("dubbo provider started");
		SpringApplication.run(PickupApplication.class, args);
	}
}
