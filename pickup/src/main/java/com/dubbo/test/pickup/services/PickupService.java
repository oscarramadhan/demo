package com.dubbo.test.pickup.services;

import com.dubbo.test.pickup.dtos.PickupDTO;

import java.util.List;

public interface PickupService {
    public List<PickupDTO> get();
}
