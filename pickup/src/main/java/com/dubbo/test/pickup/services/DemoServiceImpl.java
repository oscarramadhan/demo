package com.dubbo.test.pickup.services;

import com.dubbo.test.iface.DemoService;
import com.dubbo.test.iface.request.CreateOrderRequest;
import com.dubbo.test.pickup.entities.redis.Pickup;
import com.dubbo.test.pickup.entities.sql.TBPickup;
import com.dubbo.test.pickup.repositories.redis.PickupRedisRepository;
import com.dubbo.test.pickup.repositories.sql.PickupSQLRepository;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@DubboService(version = "1.0.0")
public class DemoServiceImpl implements DemoService {
    private final PickupRedisRepository pickupRedisRepository;
    private final PickupSQLRepository pickupSQLRepository;

    @Autowired
    private KafkaTemplate<String, CreateOrderRequest> kafkaTemplate;

    public DemoServiceImpl(PickupRedisRepository pickupRedisRepository, PickupSQLRepository pickupSQLRepository) {
        this.pickupRedisRepository = pickupRedisRepository;
        this.pickupSQLRepository = pickupSQLRepository;
    }

    @Override
    public String sayHello(String name) {
        System.out.println("Hello " + name + ", request from consumer: " + RpcContext.getServiceContext().getRemoteAddress());
        return "Hello " + name;
    }

    @Override
    public void sendOrderRequest(CreateOrderRequest createOrderRequest) {
        System.out.println(createOrderRequest);
        System.out.println("request awb : " + createOrderRequest.getAwb());
        var pickup = createPickup(createOrderRequest);
        var savedPickup = pickupRedisRepository.save(pickup);

        var tbPickup = createPickupSQL(createOrderRequest);
        var savedTbPickup = pickupSQLRepository.save(tbPickup);
        sendMessage(createOrderRequest);
    }

    private Pickup createPickup(CreateOrderRequest createOrderRequest) {
        Pickup pickup = new Pickup();
        pickup.setAwb(createOrderRequest.getAwb());
        pickup.setWeight(createOrderRequest.getWeight());
        pickup.setSenderAddress(createOrderRequest.getSenderAddress());
        pickup.setReceiverAddress(createOrderRequest.getReceiverAddress());
        pickup.setDriverId(pickup.getAwb() + "1001");

        return pickup;
    }

    private TBPickup createPickupSQL(CreateOrderRequest createOrderRequest) {
        TBPickup tbPickup = new TBPickup();
        tbPickup.setAwb(createOrderRequest.getAwb());
        tbPickup.setWeight(createOrderRequest.getWeight());
        tbPickup.setSenderAddress(createOrderRequest.getSenderAddress());
        tbPickup.setReceiverAddress(createOrderRequest.getReceiverAddress());
        tbPickup.setDriverId(tbPickup.getAwb() + "1001");

        return tbPickup;
    }

    public void sendMessage(CreateOrderRequest createOrderRequest) {
        System.out.println("-- sending message to topic --");
        System.out.println(createOrderRequest);
        kafkaTemplate.send("pickup", createOrderRequest);
    }
}
