package com.dubbo.test.pickup.repositories.redis;

import com.dubbo.test.pickup.entities.redis.Pickup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PickupRedisRepository extends CrudRepository<Pickup, String> {
}
