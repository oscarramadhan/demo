package com.dubbo.test.pickup.services;

import com.dubbo.test.pickup.dtos.PickupDTO;
import com.dubbo.test.pickup.repositories.redis.PickupRedisRepository;
import com.dubbo.test.pickup.repositories.sql.PickupSQLRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PickupServiceImpl implements PickupService {
    private final PickupRedisRepository pickupRedisRepository;
    private final PickupSQLRepository pickupSQLRepository;

    public PickupServiceImpl(PickupRedisRepository pickupRedisRepository, PickupSQLRepository pickupSQLRepository) {
        this.pickupRedisRepository = pickupRedisRepository;
        this.pickupSQLRepository = pickupSQLRepository;
    }

    @Override
    public List<PickupDTO> get() {
        var pickups = getPickupRedis();
        if (pickups.size() == 0) {
            System.out.println("redis pickup is empty, get from sql instead");
            pickups = getPickupSQL();
        }
        return pickups;
    }

    private List<PickupDTO> getPickupRedis() {
        List<PickupDTO> pickupDTOS = new ArrayList<>();
        var pickups = this.pickupRedisRepository.findAll();
        pickups.forEach(pickup -> {
            if (pickup != null) {
                PickupDTO pickupDTO = new PickupDTO();
                pickupDTO.setAwb(pickup.getAwb());
                pickupDTO.setWeight(pickup.getWeight());
                pickupDTO.setSenderAddress(pickup.getSenderAddress());
                pickupDTO.setReceiverAddress(pickup.getReceiverAddress());
                pickupDTO.setDriverId(pickup.getDriverId());

                pickupDTOS.add(pickupDTO);
            }
        });

        return pickupDTOS;
    }

    private List<PickupDTO> getPickupSQL() {
        List<PickupDTO> pickupDTOS = new ArrayList<>();
        var pickups = pickupSQLRepository.findAll();
        pickups.stream().forEach(pickup -> {
            PickupDTO pickupDTO = new PickupDTO();
            pickupDTO.setAwb(pickup.getAwb());
            pickupDTO.setWeight(pickup.getWeight());
            pickupDTO.setSenderAddress(pickup.getSenderAddress());
            pickupDTO.setReceiverAddress(pickup.getReceiverAddress());
            pickupDTO.setDriverId(pickup.getDriverId());

            pickupDTOS.add(pickupDTO);
        });

        return pickupDTOS;
    }
}
