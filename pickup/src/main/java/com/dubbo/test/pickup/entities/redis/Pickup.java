package com.dubbo.test.pickup.entities.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RedisHash(value = "Pickup")
public class Pickup {
    @Id
    private String awb;
    private double weight;
    private String senderAddress;
    private String receiverAddress;
    private String driverId;
}
