package com.dubbo.test.pickup.controllers;

import com.dubbo.test.pickup.dtos.PickupDTO;
import com.dubbo.test.pickup.services.PickupService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/pickups")
public class PickupController {
    private final PickupService pickupService;

    public PickupController(PickupService pickupService) {
        this.pickupService = pickupService;
    }

    @GetMapping
    public ResponseEntity<List<PickupDTO>> get() {
        var pickups = this.pickupService.get();
        return ResponseEntity.ok(pickups);
    }
}
