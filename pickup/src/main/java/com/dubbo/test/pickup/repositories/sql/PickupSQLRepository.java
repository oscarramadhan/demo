package com.dubbo.test.pickup.repositories.sql;

import com.dubbo.test.pickup.entities.sql.TBPickup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PickupSQLRepository extends JpaRepository<TBPickup, String> {

}
