# demo



## Description

This project is example project about dubbo-rpc, redis-cache, and kafka-spring.

Please check on each of those items that mention in their own documentation to get the idea about this project.

## to start project's requirements

list of requirements needed for this project : 
- [x] Docker (to use docker-compose to initiate project requirements)
- [x] Zookeeper
- [x] Redis
- [x] PostgreSQL
- [x] Kafka

to start project's requirements :
```
docker-compose up -d
```
make sure you're on the right path when execute command above.