package com.dubbo.test.delivery.entities.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RedisHash(value = "Delivery")
public class Delivery {
    @Id
    private String awb;
    private double weight;
    private String senderAddress;
    private String receiverAddress;
    private String driverId;
}
