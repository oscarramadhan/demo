package com.dubbo.test.delivery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliveryApplication {
    public static void main(String[] args) {
        System.out.println("delivery started");
        SpringApplication.run(DeliveryApplication.class, args);
    }
}
