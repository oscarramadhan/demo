package com.dubbo.test.delivery.repositories.redis;

import com.dubbo.test.delivery.entities.redis.Delivery;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryRedisRepository extends CrudRepository<Delivery, String> {
}
