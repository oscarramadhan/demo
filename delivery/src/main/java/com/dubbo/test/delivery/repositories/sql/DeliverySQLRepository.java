package com.dubbo.test.delivery.repositories.sql;

import com.dubbo.test.delivery.entities.sql.TBDelivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliverySQLRepository extends JpaRepository<TBDelivery, String> {
}
