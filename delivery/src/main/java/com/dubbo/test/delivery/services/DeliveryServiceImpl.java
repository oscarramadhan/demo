package com.dubbo.test.delivery.services;

import com.dubbo.test.delivery.dtos.DeliveryDTO;
import com.dubbo.test.delivery.entities.redis.Delivery;
import com.dubbo.test.delivery.entities.sql.TBDelivery;
import com.dubbo.test.delivery.repositories.redis.DeliveryRedisRepository;
import com.dubbo.test.delivery.repositories.sql.DeliverySQLRepository;
import com.dubbo.test.iface.request.CreateOrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeliveryServiceImpl implements DeliveryService{
    @Autowired
    private DeliveryRedisRepository deliveryRedisRepository;

    @Autowired
    private DeliverySQLRepository deliverySQLRepository;

    @Override
    public List<DeliveryDTO> get() {
        var deliveries = getDeliveryRedis();
        if (deliveries.size() == 0) {
            System.out.println("redis delivery is empty, get from sql instead");
            deliveries = getDeliverySQL();
        }
        return deliveries;
    }

    @KafkaListener(topics = "pickup", groupId = "group-delivery", containerFactory = "customListenerContainerFactory")
    public void listen(CreateOrderRequest message) {
        System.out.println("-- received message --");
        System.out.println("awb: " + message.getAwb());
        System.out.println(message);
        var delivery = createDelivery(message);
        var savedDelivery = deliveryRedisRepository.save(delivery);

        var tbDelivery = createDeliverySQL(message);
        var savedTbDelivery = deliverySQLRepository.save(tbDelivery);
    }

    private List<DeliveryDTO> getDeliveryRedis() {
        List<DeliveryDTO> deliveryDTOS = new ArrayList<>();
        var deliveries = this.deliveryRedisRepository.findAll();
        deliveries.forEach(delivery -> {
            if (delivery != null) {
                DeliveryDTO deliveryDTO = new DeliveryDTO();
                deliveryDTO.setAwb(delivery.getAwb());
                deliveryDTO.setWeight(delivery.getWeight());
                deliveryDTO.setSenderAddress(delivery.getSenderAddress());
                deliveryDTO.setReceiverAddress(delivery.getReceiverAddress());
                deliveryDTO.setDriverId(delivery.getDriverId());

                deliveryDTOS.add(deliveryDTO);
            }
        });

        return deliveryDTOS;
    }

    private List<DeliveryDTO> getDeliverySQL() {
        List<DeliveryDTO> deliveryDTOS = new ArrayList<>();
        var deliveries = deliverySQLRepository.findAll();
        deliveries.stream().forEach(delivery -> {
            DeliveryDTO deliveryDTO = new DeliveryDTO();
            deliveryDTO.setAwb(delivery.getAwb());
            deliveryDTO.setWeight(delivery.getWeight());
            deliveryDTO.setSenderAddress(delivery.getSenderAddress());
            deliveryDTO.setReceiverAddress(delivery.getReceiverAddress());
            deliveryDTO.setDriverId(delivery.getDriverId());

            deliveryDTOS.add(deliveryDTO);
        });

        return deliveryDTOS;
    }

    private Delivery createDelivery(CreateOrderRequest createOrderRequest) {
        Delivery delivery = new Delivery();
        delivery.setAwb(createOrderRequest.getAwb());
        delivery.setWeight(createOrderRequest.getWeight());
        delivery.setSenderAddress(createOrderRequest.getSenderAddress());
        delivery.setReceiverAddress(createOrderRequest.getReceiverAddress());
        delivery.setDriverId(delivery.getAwb() + "1001");

        return delivery;
    }

    private TBDelivery createDeliverySQL(CreateOrderRequest createOrderRequest) {
        TBDelivery tbPickup = new TBDelivery();
        tbPickup.setAwb(createOrderRequest.getAwb());
        tbPickup.setWeight(createOrderRequest.getWeight());
        tbPickup.setSenderAddress(createOrderRequest.getSenderAddress());
        tbPickup.setReceiverAddress(createOrderRequest.getReceiverAddress());
        tbPickup.setDriverId(tbPickup.getAwb() + "1001");

        return tbPickup;
    }
}
