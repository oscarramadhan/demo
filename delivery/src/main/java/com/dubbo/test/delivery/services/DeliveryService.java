package com.dubbo.test.delivery.services;

import com.dubbo.test.delivery.dtos.DeliveryDTO;

import java.util.List;

public interface DeliveryService {
    List<DeliveryDTO> get();
}
