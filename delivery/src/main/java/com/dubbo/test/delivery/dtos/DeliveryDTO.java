package com.dubbo.test.delivery.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryDTO {
    private String awb;
    private double weight;
    private String senderAddress;
    private String receiverAddress;
    private String driverId;
}