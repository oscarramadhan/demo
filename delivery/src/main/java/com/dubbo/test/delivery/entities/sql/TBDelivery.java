package com.dubbo.test.delivery.entities.sql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_delivery", schema = "onboarding")
public class TBDelivery {
    @Id
    private String awb;

    @Column(name = "weight", scale = 2, precision = 8)
    private double weight;

    @Column(name = "sender_address")
    private String senderAddress;

    @Column(name = "receiver_address")
    private String receiverAddress;

    @Column(name = "driver_id")
    private String driverId;
}
