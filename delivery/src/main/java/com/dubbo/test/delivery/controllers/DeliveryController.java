package com.dubbo.test.delivery.controllers;

import com.dubbo.test.delivery.dtos.DeliveryDTO;
import com.dubbo.test.delivery.services.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/deliveries")
public class DeliveryController {
    @Autowired
    private DeliveryService deliveryService;

    @GetMapping
    public ResponseEntity<List<DeliveryDTO>> get() {
        var pickups = this.deliveryService.get();
        return ResponseEntity.ok(pickups);
    }
}
