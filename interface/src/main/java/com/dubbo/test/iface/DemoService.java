package com.dubbo.test.iface;

import com.dubbo.test.iface.request.CreateOrderRequest;

public interface DemoService {
    String sayHello(String name);
    void sendOrderRequest(CreateOrderRequest createOrderRequest);
}
