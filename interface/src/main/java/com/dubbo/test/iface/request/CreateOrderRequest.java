package com.dubbo.test.iface.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateOrderRequest implements Serializable {
    private String awb;
    private double weight;
    private String senderAddress;
    private String receiverAddress;
}
