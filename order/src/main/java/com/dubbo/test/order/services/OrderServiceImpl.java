package com.dubbo.test.order.services;

import com.dubbo.test.order.entities.redis.Order;
import com.dubbo.test.order.entities.sql.TBOrder;
import com.dubbo.test.order.repositories.redis.OrderRedisRepository;
import com.dubbo.test.order.repositories.sql.OrderSQLRepository;
import com.dubbo.test.iface.DemoService;
import com.dubbo.test.iface.request.CreateOrderRequest;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService{
    @Autowired
    private OrderRedisRepository orderRedisRepository;

    @Autowired
    private OrderSQLRepository orderSQLRepository;

    @DubboReference(
            version = "1.0.0",
            url = "dubbo://127.0.0.1:12345",
            timeout = 1000
    )
    public DemoService demoService;

    @Override
    public Order create(CreateOrderRequest createOrderRequest) {
        Order order = new Order();
        order.setAwb(createOrderRequest.getAwb());
        order.setWeight(createOrderRequest.getWeight());
        order.setSenderAddress(createOrderRequest.getSenderAddress());
        order.setReceiverAddress(createOrderRequest.getReceiverAddress());

        var savedOrder = orderRedisRepository.save(order);
        demoService.sendOrderRequest(createOrderRequest);

        var tbOrder = createOrderSQL(createOrderRequest);
        var savedTbOrder = orderSQLRepository.save(tbOrder);

        return savedOrder;
    }

    @Override
    public Order getById(String id) {
        var order = orderRedisRepository.findById(id);
        return order.isPresent()? order.get() : null;
    }

    public TBOrder createOrderSQL(CreateOrderRequest createOrderRequest) {
        TBOrder tbOrder = new TBOrder();
        tbOrder.setAwb(createOrderRequest.getAwb());
        tbOrder.setWeight(createOrderRequest.getWeight());
        tbOrder.setReceiverAddress(createOrderRequest.getReceiverAddress());
        tbOrder.setSenderAddress(createOrderRequest.getSenderAddress());

        return tbOrder;
    }
}
