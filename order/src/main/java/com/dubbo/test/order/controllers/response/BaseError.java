package com.dubbo.test.order.controllers.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class BaseError {
    @JsonInclude()
    private final int status;

    @JsonInclude()
    private final String message;

    public BaseError(int status, String message) {
        this.status = status;
        this.message = message;
    }
}
