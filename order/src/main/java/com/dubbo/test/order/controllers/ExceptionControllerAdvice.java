package com.dubbo.test.order.controllers;

import com.dubbo.test.order.controllers.response.BaseError;
import com.dubbo.test.order.controllers.response.BaseErrorResponse;
import com.dubbo.test.order.exceptions.AlreadyExistException;
import com.dubbo.test.order.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerAdvice {
    private static final String GENERIC_ERROR = "Couldn't process request";

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<BaseErrorResponse> handleGenericError(Throwable t) {
        System.out.println("ERROR!! cause : " + t.getMessage());
        return new ResponseEntity<>(
                new BaseErrorResponse(
                        new BaseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), GENERIC_ERROR)
                ), HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<BaseErrorResponse> handleNotFoundException(NotFoundException e) {
        return new ResponseEntity<>(
                new BaseErrorResponse(
                        new BaseError(HttpStatus.NOT_FOUND.value(), e.getMessage())
                ), HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(AlreadyExistException.class)
    public ResponseEntity<BaseErrorResponse> handleAlreadyExistException(AlreadyExistException e) {
        return new ResponseEntity<>(
                new BaseErrorResponse(
                        new BaseError(HttpStatus.BAD_REQUEST.value(), e.getMessage())
                ), HttpStatus.BAD_REQUEST
        );
    }
}
