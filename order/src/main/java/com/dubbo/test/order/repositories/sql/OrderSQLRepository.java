package com.dubbo.test.order.repositories.sql;

import com.dubbo.test.order.entities.sql.TBOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderSQLRepository extends JpaRepository<TBOrder, String> {
}
