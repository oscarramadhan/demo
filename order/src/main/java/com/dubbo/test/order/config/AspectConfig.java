package com.dubbo.test.order.config;

import com.dubbo.test.order.exceptions.AlreadyExistException;
import com.dubbo.test.order.services.OrderService;
import com.dubbo.test.iface.request.CreateOrderRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectConfig {
    @Autowired
    private OrderService orderService;

    @Before("execution(* com.dubbo.test.order.services.OrderServiceImpl.create(..))")
    public void before(JoinPoint joinPoint) throws AlreadyExistException {
        System.out.println("-- before exe --");
        var orderRequest = CreateOrderRequest.class.cast(joinPoint.getArgs()[0]);
        System.out.println(orderRequest);
        var existAwb = orderService.getById(orderRequest.getAwb());
        if (existAwb != null) {
            System.out.println("awb : " + orderRequest.getAwb() + " already exist!");
            throw new AlreadyExistException("awb : " + orderRequest.getAwb() + " already exist!");
        }
    }
}
