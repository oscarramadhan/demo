package com.dubbo.test.order.services;

import com.dubbo.test.order.entities.redis.Order;
import com.dubbo.test.iface.request.CreateOrderRequest;

public interface OrderService {
    Order create(CreateOrderRequest createOrderRequest);
    Order getById(String id);
}
