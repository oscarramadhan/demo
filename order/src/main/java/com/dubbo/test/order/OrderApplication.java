package com.dubbo.test.order;

import com.dubbo.test.iface.DemoService;
import org.apache.dubbo.config.annotation.DubboReference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class OrderApplication {
	@DubboReference(
		version = "1.0.0",
		url = "dubbo://127.0.0.1:12345",
		timeout = 1000
	)
	private DemoService demoService;

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(OrderApplication.class, args);
		OrderApplication application = context.getBean(OrderApplication.class);
		String result = application.doSayHello("consumer");
		System.out.println("result: " + result);
	}

	public String doSayHello(String name) {
		return demoService.sayHello(name);
	}
}
