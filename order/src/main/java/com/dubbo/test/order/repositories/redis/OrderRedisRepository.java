package com.dubbo.test.order.repositories.redis;

import com.dubbo.test.order.entities.redis.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRedisRepository extends CrudRepository<Order, String> {
}
