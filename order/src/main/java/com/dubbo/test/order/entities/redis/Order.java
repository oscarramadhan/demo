package com.dubbo.test.order.entities.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RedisHash(value = "Order", timeToLive = 20)
public class Order implements Serializable {
    @Id
    private String awb;
    private double weight;
    private String senderAddress;
    private String receiverAddress;
}
