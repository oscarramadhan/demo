package com.dubbo.test.order.controllers;

import com.dubbo.test.order.entities.redis.Order;
import com.dubbo.test.order.exceptions.AlreadyExistException;
import com.dubbo.test.order.exceptions.NotFoundException;
import com.dubbo.test.order.services.OrderService;
import com.dubbo.test.iface.request.CreateOrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseEntity<Order> create(
            @RequestBody CreateOrderRequest createOrderRequest
    ) throws AlreadyExistException {
        Order exist = orderService.getById(createOrderRequest.getAwb());
        if (exist != null) {
            throw new AlreadyExistException("AWB already exist!");
        }
        Order order = orderService.create(createOrderRequest);
        return ResponseEntity.ok(order);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> get(@PathVariable String id) throws NotFoundException {
        Order order = orderService.getById(id);
        if (order == null) {
            throw new NotFoundException("AWB doesn't exist!");
        }
        return ResponseEntity.ok(order);
    }
}
